package be.janolaerts.enterprisespringopdrachten.restclient;

import be.janolaerts.enterprisespringopdrachten.domain.Message;

import java.net.URI;

public interface MessagesClient {

    Message getMessageById(int id);
    URI createMessage(Message message);
    void updateMessage(Message message);
    Message patchMessage(int id, String text);
    void deleteMessage(int id);
}