package be.janolaerts.enterprisespringopdrachten.repository;

import be.janolaerts.enterprisespringopdrachten.domain.Message;

import java.util.List;

public interface MessageRepository {

    Message getMessageById(int id);
    List<Message> getAllMessages();
    List<Message> getMessagesByAuthor(String author);
    Message createMessage(Message message);
    Message updateMessage(Message message);
    void deleteMessage(int id);
}