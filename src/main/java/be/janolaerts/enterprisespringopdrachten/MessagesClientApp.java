package be.janolaerts.enterprisespringopdrachten;

import be.janolaerts.enterprisespringopdrachten.domain.Message;
import be.janolaerts.enterprisespringopdrachten.restclient.MessagesClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URL;

// Todo: ask Ward: how to validate? (@NotBlank does not work)

@SpringBootApplication
public class MessagesClientApp {

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.requestFactory(
                HttpComponentsClientHttpRequestFactory::new).build();
    }

    public static void main(String[] args) throws Exception {

        ConfigurableApplicationContext ctx =
                SpringApplication.run(MessagesClientApp.class, args);

        MessagesClient messagesClient = ctx.getBean("messagesRestClient", MessagesClient.class);

        Message message;

        // Get message
        message = messagesClient.getMessageById(1);
        System.out.println(message.getDisplayMessage());

        // Post message
        message = new Message(0, "Jan", "Post Test");
        messagesClient.createMessage(message);
        message = messagesClient.getMessageById(3);
        System.out.println(message.getDisplayMessage());

        // Put message
        message = messagesClient.getMessageById(3);
        message.setText("Put Test");
        messagesClient.updateMessage(message);
        System.out.println(message.getDisplayMessage());

        // Patch message
        messagesClient.patchMessage(3, "Patch Test");
        message = messagesClient.getMessageById(2);
        System.out.println(message.getDisplayMessage());

        // Delete message
        messagesClient.deleteMessage(3);
    }
}