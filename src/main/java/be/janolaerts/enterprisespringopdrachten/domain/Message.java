package be.janolaerts.enterprisespringopdrachten.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.NotBlank;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.StringJoiner;

public class Message implements Serializable {

    private int id;

    @NotBlank(message="author cannot be blank and must not be null")
    private String author;

    @NotBlank(message="text cannot be blank and must not be null")
    private String text;

    public Message() {
    }

    public Message(int id, String author, String text) {
        this.id = id;
        this.author = author;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @XmlTransient
    @JsonIgnore
    public String getDisplayMessage() {
        return author + " : " + text;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Message.class.getSimpleName() + "[", "]")
                .add("id=" + id).add("author='" + author + "'")
                .add("text='" + text + "'").toString();
    }
}